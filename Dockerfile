FROM maven:3.6-openjdk-8
WORKDIR /usr/src/serenity-jbehave-starter-selenium-grid
COPY pom.xml .
RUN mvn dependency:go-offline
COPY . .
RUN mvn package
CMD mvn verify
